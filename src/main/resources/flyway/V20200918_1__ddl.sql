
create table if not exists authorizationn
(
    auth_id                       integer default 8
        constraint auth_id
            unique,
    username                      varchar default 255,
    email                         varchar default 255,
    password                      varchar default 128,
    role                          varchar default 255,
    forgot_password_key           varchar default 128,
    forgot_password_key_timestamp integer default 8,
    company_unit_id               integer default 8
);

alter table authorizationn
    owner to postgres;


create table if not exists users
(
    user_id              bigint not null
        constraint users_pkey
            primary key,
    auth_id              bigint,
    name                 varchar(128),
    fullname             varchar(128),
    surname              varchar(128),
    secondname           varchar(128),
    status               varchar(128),
    company_unit_id      bigint,
    password             varchar(128),
    last_login_timestamp bigint,
    iin                  varchar(32),
    is_active            boolean,
    is_activated         boolean,
    created_timestamp    bigint,
    created_by           bigint,
    updated_timestamp    bigint,
    updated_by           bigint
);

alter table users
    owner to postgres;

create table if not exists  fond
(
    fond_id           integer default 8
        constraint fond_id
            unique,
    fond_number       varchar default 128,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table fond
    owner to postgres;

create table if not exists company
(
    company_id        bigint not null
        constraint company_pkey
            primary key,
    name_ru           varchar default 128,
    name_kz           varchar default 128,
    name_en           varchar default 128,
    bin               varchar default 32,
    parent_id         bigint,
    fond_id           bigint,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table company
    owner to postgres;

create table if not exists company_unit
(
    company_unit_id   bigint not null
        constraint company_unit_pkey
            primary key,
    name_ru           varchar default 128,
    name_kz           varchar default 128,
    name_en           varchar default 128,
    parent_id         bigint,
    year              integer,
    company_id        integer,
    code_index        varchar default 16,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table company_unit
    owner to postgres;

create table if not exists request
(
    request_id        bigint not null
        constraint request_pkey
            primary key,
    request_user_id   bigint,
    response_user_id  bigint,
    case_id           bigint,
    case_index_id     bigint,
    created_type      varchar default 64,
    comment           varchar default 255,
    status            varchar default 64,
    timestamp         bigint,
    sharestart        bigint,
    sharefinish       bigint,
    favorite          boolean,
    updated_timestamp integer default 8,
    updated_by        integer default 8,
    declinenote       varchar default 255,
    company_unit_id   bigint,
    from_request_id   bigint
);

alter table request
    owner to postgres;

create table if not exists share
(
    id              bigint not null
        constraint share_pkey
            primary key,
    request_id      bigint,
    note            varchar default 255,
    sender_id       bigint,
    receiver_id     bigint,
    share_timestamp bigint
);

alter table share
    owner to postgres;

create table if not exists history_status_request
(
    id                bigint not null
        constraint history_status_request_pkey
            primary key,
    request_id        bigint,
    status            varchar default 64,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table history_status_request
    owner to postgres;

create table if not exists catalog
(
    catalog_id        bigint not null
        constraint catalog_pkey
            primary key,
    name_ru           varchar default 128,
    name_kz           varchar default 128,
    name_en           varchar default 128,
    parent_id         bigint,
    company_unit_id   bigint,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table catalog
    owner to postgres;

create table if not exists catalog_case
(
    catalog_case_id   bigint not null
        constraint catalog_case_pkey
            primary key,
    case_id           bigint,
    catalog_id        bigint,
    company_unit_id   bigint,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table catalog_case
    owner to postgres;

create table if not exists activity_journal
(
    id                bigint not null
        constraint activity_journal_pkey
            primary key,
    event_type        varchar default 128,
    object_type       varchar default 255,
    object_id         bigint,
    created_timestamp bigint,
    created_by        bigint,
    message_level     varchar default 128,
    message           varchar default 255
);

alter table activity_journal
    owner to postgres;

create table if not exists location
(
    location_id       bigint not null
        constraint location_pkey
            primary key,
    row               varchar default 64,
    line              varchar default 64,
    columnn           varchar default 64,
    box               varchar default 64,
    company_unit_id   bigint,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table location
    owner to postgres;

create  table if not exists notification
(
    id                bigint not null
        constraint notification_pkey
            primary key,
    object_type       varchar default 128,
    object_id         bigint,
    company_unit_id   bigint,
    user_id           bigint,
    created_timestamp bigint,
    viewed_timestamp  bigint,
    is_viewed         boolean,
    title             varchar default 255,
    text              varchar default 255,
    company_id        bigint
);

alter table notification
    owner to postgres;

create table if not exists tempfiles
(
    id               bigint not null
        constraint tempfiles_pkey
            primary key,
    file_binary      text,
    file_binary_byte bytea
);

alter table tempfiles
    owner to postgres;

create table if not exists file
(
    file_id           bigint not null
        constraint file_pkey
            primary key,
    name              varchar default 128,
    type              varchar default 128,
    size              bigint,
    page_count        integer,
    hash              varchar default 128,
    is_deleted        boolean,
    file_binary_id    bigint,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table file
    owner to postgres;

create table if not exists file_routing
(
    id         bigint not null
        constraint file_routing_pkey
            primary key,
    file_id    bigint,
    table_name varchar default 128,
    table_id   bigint,
    type       varchar default 128
);

alter table file_routing
    owner to postgres;

create table if not exists searchkey
(
    id                bigint not null
        constraint searchkey_pkey
            primary key,
    name              varchar default 128,
    company_unit_id   bigint,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table searchkey
    owner to postgres;

create table if not exists search_key_routing
(
    id            bigint not null
        constraint search_key_routing_pkey
            primary key,
    search_key_id bigint,
    table_name    varchar default 128,
    table_id      bigint,
    type          varchar default 128
);

alter table search_key_routing
    owner to postgres;

create table if not exists record
(
    id                bigint not null
        constraint record_pkey
            primary key,
    number            varchar default 128,
    type              varchar default 128,
    company_unit_id   bigint,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table record
    owner to postgres;

create table if not exists nomenclature
(
    id                      bigint not null
        constraint nomenclature_pkey
            primary key,
    nomenclature_number     varchar default 128,
    year                    integer,
    nomenclature_summary_id bigint,
    company_unit_id         bigint,
    created_timestamp       integer default 8,
    created_by              integer default 8,
    updated_timestamp       integer default 8,
    updated_by              integer default 8
);

alter table nomenclature
    owner to postgres;

create table if not exists case_index
(
    id                bigint not null
        constraint case_index_pkey
            primary key,
    case_index        varchar default 128,
    title_ru          varchar default 128,
    title_kz          varchar default 128,
    title_en          varchar default 128,
    storage_type      integer,
    storage_year      integer,
    note              varchar default 128,
    company_unit_id   bigint,
    nomenclature_id   bigint,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table case_index
    owner to postgres;

create table if not exists nomenclature_summary
(
    id                bigint not null
        constraint nomenclature_summary_pkey
            primary key,
    number            varchar default 128,
    year              integer,
    company_unit_id   bigint,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table nomenclature_summary
    owner to postgres;

create table if not exists desrtuction_act
(
    id                bigint not null
        constraint desrtuction_act_pkey
            primary key,
    number            varchar default 128,
    subject           varchar default 258,
    company_unit_id   bigint,
    created_timestamp integer default 8,
    created_by        integer default 8,
    updated_timestamp integer default 8,
    updated_by        integer default 8
);

alter table desrtuction_act
    owner to postgres;

create table if not exists cases
(
    id                      bigint not null
        constraint cases_pkey
            primary key,
    case_number             varchar default 128,
    volume_number           varchar default 128,
    title_ru                varchar default 128,
    title_kz                varchar default 128,
    title_en                varchar default 128,
    start_date              bigint,
    finish_date             bigint,
    page_count              bigint,
    subscription_eds        boolean,
    eds                     text,
    dekivery_naf            boolean,
    is_deleted              boolean,
    limited_access          boolean,
    hash                    varchar default 128,
    version                 integer,
    version_id              varchar default 128,
    is_active_version       boolean,
    note                    varchar default 255,
    location_id             bigint,
    case_idex_id            bigint,
    record                  bigint,
    destruction_act         bigint,
    company_unit_id         bigint,
    blockchain_address      varchar default 128,
    blockchain_include_date bigint,
    created_timestamp       integer default 8,
    created_by              integer default 8,
    updated_timestamp       integer default 8,
    updated_by              integer default 8
);

alter table cases
    owner to postgres;

