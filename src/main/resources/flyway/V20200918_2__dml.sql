INSERT INTO public.activity_journal (id, event_type, object_type, object_id, created_timestamp, created_by,
                                     message_level, message)
VALUES (3, 'qwerty', 'qwerty', 1, 1, 1, 'submited', 'hi');

INSERT INTO public.activity_journal (id, event_type, object_type, object_id, created_timestamp, created_by,
                                     message_level, message)
VALUES (4, 'qwerty2', 'qwerty2', 2, 2, 2, 'not submited', 'welcome');



INSERT INTO public.authorizationn (auth_id, username, email, password, role, forgot_password_key,
                                   forgot_password_key_timestamp, company_unit_id)
VALUES (3, 'wvcz', 'wvcz', '1', 'student', 'name', 12, 1);

INSERT INTO public.authorizationn (auth_id, username, email, password, role, forgot_password_key,
                                   forgot_password_key_timestamp, company_unit_id)
VALUES (4, 'bota', 'bota', '2', 'child', 'coconut', 122, 2);

INSERT INTO public.fond (fond_id, fond_number, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (3, '1', 11, 11, 13, 13);

INSERT INTO public.fond (fond_id, fond_number, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (99, 99, 22, 22, 122, 122);

INSERT INTO users (user_id, auth_id, name, fullname, surname, secondname, status, company_unit_id, password,
                          last_login_timestamp, iin, is_active, is_activated, created_timestamp, created_by,
                          updated_timestamp, updated_by)
VALUES (3, 2, 'wvcz', 'Askarbekkyzy Akbota', 'Askarbekkyzy', 'Aqbota', 'student', 1, '1234', 123, '00111100999', false,
        true, 122222, 122, 122, 123);

INSERT INTO users (user_id, auth_id, name, fullname, surname, secondname, status, company_unit_id, password,
                          last_login_timestamp, iin, is_active, is_activated, created_timestamp, created_by,
                          updated_timestamp, updated_by)
VALUES (6, 2, 'assyl', ' Rakhmetova Assyl', 'Rakhmeetova', 'Assyl', 'married', 2, '1111', 122, '122222', true, false,
        1222, 12222, 1222, 1233);

INSERT INTO public.tempfiles (id, file_binary, file_binary_byte)
VALUES (7, '1', 'LOB');

INSERT INTO public.tempfiles (id, file_binary, file_binary_byte)
VALUES (9, '2', 'LOB');

INSERT INTO public.tempfiles (id, file_binary, file_binary_byte)
VALUES (99, '3', 'LOB');

INSERT INTO public.request (request_id, request_user_id, response_user_id, case_id, case_index_id, created_type,
                            comment, status, timestamp, sharestart, sharefinish, favorite, updated_timestamp,
                            updated_by, declinenote, company_unit_id, from_request_id)
VALUES (99, 1, 1, 1, 1, 'file', 'vaaarch', 'done', 1, 1, 1, false, 1, 1, 'dddd', 1, 1);

INSERT INTO public.request (request_id, request_user_id, response_user_id, case_id, case_index_id, created_type,
                            comment, status, timestamp, sharestart, sharefinish, favorite, updated_timestamp,
                            updated_by, declinenote, company_unit_id, from_request_id)
VALUES (999, 2, 2, 2, 2, 'pdf', 'sss', 'sss', 222, 22, 22, true, 22, 2, 'ddddd', 2, 2);

INSERT INTO public.catalog (catalog_id, name_ru, name_kz, name_en, parent_id, company_unit_id, created_timestamp,
                            created_by, updated_timestamp, updated_by)
VALUES (991, 'старт', 'старт', 'start', 1, 1, 1, 1, 1, 1);

INSERT INTO public.catalog (catalog_id, name_ru, name_kz, name_en, parent_id, company_unit_id, created_timestamp,
                            created_by, updated_timestamp, updated_by)
VALUES (111, 'финиш', 'финиш', 'finish', 2, 2, 22, 22, 22, 2);

INSERT INTO public.company (company_id, name_ru, name_kz, name_en, bin, parent_id, fond_id, created_timestamp,
                            created_by, updated_timestamp, updated_by)
VALUES (45, 'стар', 'стар', 'star', '1234', 1, 1, 123, 123, 123, 1);

INSERT INTO public.company (company_id, name_ru, name_kz, name_en, bin, parent_id, fond_id, created_timestamp,
                            created_by, updated_timestamp, updated_by)
VALUES (945, 'хелмиш', 'хелмиш', 'helmish', '1234444', 2, 8, 222, 222, 222, 2222);

INSERT INTO public.company_unit (company_unit_id, name_ru, name_kz, name_en, parent_id, year, company_id, code_index,
                                 created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (9233, 'tt', 'tt', 'tt', 1, 2020, 1, '1', 111, 11, 11, 11);

INSERT INTO public.company_unit (company_unit_id, name_ru, name_kz, name_en, parent_id, year, company_id, code_index,
                                 created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (930, 'ee', 'ee', 'ee', 2, 2019, 2, '2', 222, 22, 22, 22);

INSERT INTO public.file (file_id, name, type, size, page_count, hash, is_deleted, file_binary_id, created_timestamp,
                         created_by, updated_timestamp, updated_by)
VALUES (9333, 'qww', 'qww', 1, 11, 'jfjfjf', true, 1, 22, 2, 22, 1);

INSERT INTO public.file (file_id, name, type, size, page_count, hash, is_deleted, file_binary_id, created_timestamp,
                         created_by, updated_timestamp, updated_by)
VALUES (977, 'f', 'f', 2, 233, 'fff', false, 2, 33, 33, 33, 33);

INSERT INTO public.file_routing (id, file_id, table_name, table_id, type)
VALUES (96, 1, 'w', 1, 'w'),
 (91, 2, 'q', 2, 'w');
INSERT INTO public.history_status_request (id, request_id, status, created_timestamp, created_by, updated_timestamp,
                                           updated_by)
VALUES (9, 1, 'w', 1, 1, 1, 1);

INSERT INTO public.history_status_request (id, request_id, status, created_timestamp, created_by, updated_timestamp,
                                           updated_by)
VALUES (91, 2, 'www', 2, 2, 2, 2);

INSERT INTO public.notification (id, object_type, object_id, company_unit_id, user_id, created_timestamp,
                                 viewed_timestamp, is_viewed, title, text, company_id)
VALUES (92, 'ww', 1, 1, 1, 1, 1, true, '1ee', 'edd', 1);

INSERT INTO public.notification (id, object_type, object_id, company_unit_id, user_id, created_timestamp,
                                 viewed_timestamp, is_viewed, title, text, company_id)
VALUES (93, 'eeee', 2, 2, 2, 2, 2, false, 'dwcd', 'cdwd', 2);
INSERT INTO public.share (id, request_id, note, sender_id, receiver_id, share_timestamp)
VALUES (94, 1, 'f', 1, 1, 1);

INSERT INTO public.share (id, request_id, note, sender_id, receiver_id, share_timestamp)
VALUES (95, 2, 'dd', 2, 2, 2);

INSERT INTO public.nomenclature_summary (id, number, year, company_unit_id, created_timestamp, created_by,
                                         updated_timestamp, updated_by)
VALUES (96, '1', 1999, 1, 1, 1, 1, 1);

INSERT INTO public.nomenclature_summary (id, number, year, company_unit_id, created_timestamp, created_by,
                                         updated_timestamp, updated_by)
VALUES (97, '2', 2002, 2, 2, 2, 2, 2);

INSERT INTO public.nomenclature (id, nomenclature_number, year, nomenclature_summary_id, company_unit_id,
                                 created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (98, '1', 2010, 1, 1, 1, 21, 1, 1);

INSERT INTO public.nomenclature (id, nomenclature_number, year, nomenclature_summary_id, company_unit_id,
                                 created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (99, '2', 2005, 2, 2, 1, 2, 2, 2);

INSERT INTO public.catalog_case (catalog_case_id, case_id, catalog_id, company_unit_id, created_timestamp, created_by,
                                 updated_timestamp, updated_by)
VALUES (923, 1, 1, 1, 1, 1, 1, 1);

INSERT INTO public.catalog_case (catalog_case_id, case_id, catalog_id, company_unit_id, created_timestamp, created_by,
                                 updated_timestamp, updated_by)
VALUES (9333, 2, 2, 2, 2, 2, 2, 2);

INSERT INTO public.desrtuction_act (id, number, subject, company_unit_id, created_timestamp, created_by,
                                    updated_timestamp, updated_by)
VALUES (900, '1', 'ss', 1, 1, 1, 1, 1);

INSERT INTO public.desrtuction_act (id, number, subject, company_unit_id, created_timestamp, created_by,
                                    updated_timestamp, updated_by)
VALUES (9005, '2', 'dd', 2, 2, 22, 22, 222);

INSERT INTO public.location (location_id, row, line, columnn, box, company_unit_id, created_timestamp, created_by,
                             updated_timestamp, updated_by)
VALUES (91, '1', '1', '1', '1', 1, 1, 1, 1, 1);

INSERT INTO public.location (location_id, row, line, columnn, box, company_unit_id, created_timestamp, created_by,
                             updated_timestamp, updated_by)
VALUES (92, '2', '2', '2', '2', 2, 2, 3, 3, 3);

INSERT INTO public.record (id, number, type, company_unit_id, created_timestamp, created_by, updated_timestamp,
                           updated_by)
VALUES (91, '2', '1', 1, 1, 1, 22222, 2222);

INSERT INTO public.record (id, number, type, company_unit_id, created_timestamp, created_by, updated_timestamp,
                           updated_by)
VALUES (92, '1', '2', 2, 22, 22, 222, 222);



INSERT INTO public.searchkey (id, name, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (91, '1', 1, 1, 1, 1111, 1112);

INSERT INTO public.searchkey (id, name, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (92, '2', 2, 2, 2, 222, 22222);
INSERT INTO public.search_key_routing (id, search_key_id, table_name, table_id, type)
VALUES (93, 1, '1', 1, '1');


INSERT INTO public.case_index (id, case_index, title_ru, title_kz, title_en, storage_type, storage_year, note,
                               company_unit_id, nomenclature_id, created_timestamp, created_by, updated_timestamp,
                               updated_by)
VALUES (94, '1', '1', '1', '1', 1, 2, '1', 1, 1, 1, 1, 1, 12);

INSERT INTO public.case_index (id, case_index, title_ru, title_kz, title_en, storage_type, storage_year, note,
                               company_unit_id, nomenclature_id, created_timestamp, created_by, updated_timestamp,
                               updated_by)
VALUES (97, '2', '2', '2', '2', 2, 2, '2', 2, 2, 2, 22, 22, 22);

INSERT INTO public.cases (id, case_number, volume_number, title_ru, title_kz, title_en, start_date, finish_date,
                          page_count, subscription_eds, eds, dekivery_naf, is_deleted, limited_access, hash, version,
                          version_id, is_active_version, note, location_id, case_idex_id, record, destruction_act,
                          company_unit_id, blockchain_address, blockchain_include_date, created_timestamp, created_by,
                          updated_timestamp, updated_by)
VALUES (98, '1', '1', '1', '1', '1', 1, 1, 11, true, '111111', true, true, true, '11', 1, '1', false, '1', 1, 1, 1, 1, 1,
        '1', 1, 1, 111, 11, 111);

INSERT INTO public.cases (id, case_number, volume_number, title_ru, title_kz, title_en, start_date, finish_date,
                          page_count, subscription_eds, eds, dekivery_naf, is_deleted, limited_access, hash, version,
                          version_id, is_active_version, note, location_id, case_idex_id, record, destruction_act,
                          company_unit_id, blockchain_address, blockchain_include_date, created_timestamp, created_by,
                          updated_timestamp, updated_by)
VALUES (903, '2', '2', '2', '2', '2', 2, 222, 22, true, '2222', false, false, false, '2', 2, '2', true, '2', 2, 2, 2, 2,
        9, '2', 2, 2, 2, 22, 2);









