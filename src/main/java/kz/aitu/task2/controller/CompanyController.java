package kz.aitu.task2.controller;

import kz.aitu.task2.entity.Company;
import kz.aitu.task2.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }
    @GetMapping(path = "/api/company/{company_id}")
    public ResponseEntity<?> getCompany(@PathVariable Long company_id){
        return ResponseEntity.ok(companyService.getCompany(company_id));
    }

    @GetMapping(path = "/api/company")
    public ResponseEntity<?> getAllCompany() {
        return ResponseEntity.ok(companyService.getAllCompany());
    }

    @DeleteMapping(path = "/api/company/{company_id}")
    public void deleteCompanyById(@PathVariable long company_id){
        companyService.deleteCompany(company_id);
    }
    @PutMapping(path = "/api/company")
    public ResponseEntity<?>updateCompany(@RequestBody Company company) {
        return ResponseEntity.ok(companyService.updateCompany(company));
    }
}
