package kz.aitu.task2.controller;


import kz.aitu.task2.entity.Company_unit;
import kz.aitu.task2.service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class Company_unit_Controller {
    private final CompanyUnitService companyUnitService;

    public Company_unit_Controller(CompanyUnitService companyUnitService) {
        this.companyUnitService = companyUnitService;
    }

    @GetMapping(path = "/api/companyunit/{company_unit_id}")
    public ResponseEntity<?> getCompanyUnit(@PathVariable Long company_unit_id){
        return ResponseEntity.ok(companyUnitService.getCompanyUnit(company_unit_id));
    }

    @GetMapping(path = "/api/companyunit")
    public ResponseEntity<?> getAllCompanyUnit() {
        return ResponseEntity.ok(companyUnitService.getAllCompanyUnit());
    }

    @DeleteMapping(path = "/api/companyunit/{company_unit_id}")
    public void deleteCompanyUnitById(@PathVariable long company_unit_id){
        companyUnitService.deleteCompanyUnit(company_unit_id);
    }
    @PutMapping(path = "/api/companyunit")
    public ResponseEntity<?>updateCompanyUnit(@RequestBody Company_unit company_unit) {
        return ResponseEntity.ok(companyUnitService.updateCompanyUnit(company_unit));
    }
}
