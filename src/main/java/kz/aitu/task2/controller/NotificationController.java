package kz.aitu.task2.controller;


import kz.aitu.task2.entity.Notification;
import kz.aitu.task2.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping(path = "/api/notification/{id}")
    public ResponseEntity<?> getNotification(@PathVariable Long id){
        return ResponseEntity.ok(notificationService.getNotification(id));
    }

    @GetMapping(path = "/api/notification")
    public ResponseEntity<?> getAllNotification() {
        return ResponseEntity.ok(notificationService.getAllNotification());
    }

    @DeleteMapping(path = "/api/notification/{id}")
    public void deleteNotificationById(@PathVariable long id){
        notificationService.deleteNotification(id);
    }
    @PutMapping(path = "/api/notification")
    public ResponseEntity<?>updateNotification(@RequestBody Notification notification ){
        return ResponseEntity.ok(notificationService.updateNotification(notification));
    }
}
