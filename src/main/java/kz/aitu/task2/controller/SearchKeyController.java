package kz.aitu.task2.controller;

import kz.aitu.task2.entity.Searchkey;

import kz.aitu.task2.service.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyController {
    private final SearchKeyService searchKeyService;

    public SearchKeyController(SearchKeyService searchKeyService) {
        this.searchKeyService = searchKeyService;
    }

    @GetMapping(path = "/api/searchkey/{id}")
    public ResponseEntity<?> getSearchKey(@PathVariable Long id){
        return ResponseEntity.ok(searchKeyService.getSearchKey(id));
    }

    @GetMapping(path = "/api/searchkey")
    public ResponseEntity<?> getAllSearchKey() {
        return ResponseEntity.ok(searchKeyService.getAllSearchKey());
    }

    @DeleteMapping(path = "/api/searchkey/{id}")
    public void deleteSearchKeyById(@PathVariable long id){
        searchKeyService.deleteSearchKey(id);
    }
    @PutMapping(path = "/api/searchkey")
    public ResponseEntity<?>updateSearchKey(@RequestBody Searchkey search_key ){
        return ResponseEntity.ok(searchKeyService.updateSearchKey(search_key));
    }
}
