package kz.aitu.task2.controller;
import kz.aitu.task2.entity.Activity_journal;
import kz.aitu.task2.service.ActivityJournalService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class Activity_jounal_Controller {
    private final ActivityJournalService activityJournalService;

    public Activity_jounal_Controller(ActivityJournalService activityJournalService) {
        this.activityJournalService = activityJournalService;
    }


    @GetMapping(path = "/api/journals/{id}")
    public ResponseEntity<?> getJournals(@PathVariable Long id){
        return ResponseEntity.ok(activityJournalService.getJournals(id));
    }

    @GetMapping(path = "/api/journals")
    public ResponseEntity<?> getAllJournals() {
        return ResponseEntity.ok(activityJournalService.getAllJournals());
    }

    @DeleteMapping(path = "/api/journals/{id}")
    public void deleteJournalById(@PathVariable long id){
        activityJournalService.deleteJournal(id);
    }
    @PutMapping(path = "/api/journals")
    public ResponseEntity<?>updateJournals(@RequestBody Activity_journal activity_journal) {
        return ResponseEntity.ok(activityJournalService.updateJournal(activity_journal));
    }


    }
