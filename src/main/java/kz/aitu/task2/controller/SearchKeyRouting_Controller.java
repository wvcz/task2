package kz.aitu.task2.controller;

import kz.aitu.task2.entity.Search_key_routing;

import kz.aitu.task2.service.SearchKeyRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyRouting_Controller {
    private final SearchKeyRoutingService searchKeyRoutingService;

    public SearchKeyRouting_Controller(SearchKeyRoutingService searchKeyRoutingService) {
        this.searchKeyRoutingService = searchKeyRoutingService;
    }
    @GetMapping(path = "/api/searchkeyrouting/{id}")
    public ResponseEntity<?> getSearchKeyRouting(@PathVariable Long id){
        return ResponseEntity.ok(searchKeyRoutingService.getSearchKeyRouting(id));
    }

    @GetMapping(path = "/api/searchkeyrouting")
    public ResponseEntity<?> getAllSearchKeyRouting() {
        return ResponseEntity.ok(searchKeyRoutingService.getAllSearchKeyRouting());
    }

    @DeleteMapping(path = "/api/searchkeyrouting/{id}")
    public void deleteSearchKeyRoutingById(@PathVariable long id){
        searchKeyRoutingService.deleteSearchKeyRouting(id);
    }
    @PutMapping(path = "/api/searchkeyrouting")
    public ResponseEntity<?>updateSearchKeyRouting(@RequestBody Search_key_routing search_key_routing ){
        return ResponseEntity.ok(searchKeyRoutingService.updateSearchKeyRouting(search_key_routing));
    }
}
