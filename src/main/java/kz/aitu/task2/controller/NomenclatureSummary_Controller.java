package kz.aitu.task2.controller;


import kz.aitu.task2.entity.Nomenclature_summary;
import kz.aitu.task2.service.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureSummary_Controller {
    private final NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummary_Controller(NomenclatureSummaryService nomenclatureSummaryService) {
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }
    @GetMapping(path = "/api/nomenclaturesummary/{id}")
    public ResponseEntity<?> getNomenclatureSummary(@PathVariable Long id){
        return ResponseEntity.ok(nomenclatureSummaryService.getNomenclatureSummary(id));
    }

    @GetMapping(path = "/api/nomenclaturesummary")
    public ResponseEntity<?> getAllNomenclatureSummary() {
        return ResponseEntity.ok(nomenclatureSummaryService.getAllNomenclatureSummary());
    }

    @DeleteMapping(path = "/api/nomenclaturesummary/{id}")
    public void deleteNomenclatureSummaryById(@PathVariable long id){
        nomenclatureSummaryService.deleteNomenclatureSummary(id);
    }
    @PutMapping(path = "/api/nomenclaturesummary")
    public ResponseEntity<?>update(@RequestBody Nomenclature_summary nomenclature_summary ){
        return ResponseEntity.ok(nomenclatureSummaryService.updateNomenclatureSummary(nomenclature_summary));
    }
}
