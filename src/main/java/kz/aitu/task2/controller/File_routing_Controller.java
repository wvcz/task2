package kz.aitu.task2.controller;

import kz.aitu.task2.entity.File_routing;

import kz.aitu.task2.service.FileRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
@Controller

public class File_routing_Controller {
    private final FileRoutingService fileRoutingService;

    public File_routing_Controller(FileRoutingService fileRoutingService) {
        this.fileRoutingService = fileRoutingService;
    }
    @GetMapping(path = "/api/filerouting/{id}")
    public ResponseEntity<?> getFileRouting(@PathVariable Long id){
        return ResponseEntity.ok(fileRoutingService.getFileRouting(id));
    }

    @GetMapping(path = "/api/filerouting")
    public ResponseEntity<?> getAllFileRouting() {
        return ResponseEntity.ok(fileRoutingService.getAllFileRouting());
    }

    @DeleteMapping(path = "/api/filerouting/{id}")
    public void deleteFileRoutingById(@PathVariable long id){
        fileRoutingService.deleteFileRouting(id);
    }
    @PutMapping(path = "/api/filerouting")
    public ResponseEntity<?>updateFileRouting(@RequestBody File_routing file_routing) {
        return ResponseEntity.ok(fileRoutingService.updateFileRouting(file_routing));
    }
}
