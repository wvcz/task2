package kz.aitu.task2.controller;

import kz.aitu.task2.entity.Users;
import kz.aitu.task2.service.UserService;
import org.apache.catalina.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path = "/api/users/{user_id}")
    public ResponseEntity<?>getUsers(@PathVariable Long user_id){
        return ResponseEntity.ok(userService.getUsers(user_id));
    }

    @GetMapping(path = "/api/users")
    public ResponseEntity<?> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }

    @DeleteMapping(path = "/api/users/{user_id}")
    public void deleteUserById(@PathVariable long user_id){
        userService.deleteUser(user_id);
    }
    @PutMapping(path = "/api/users")
    public ResponseEntity<?>updateUsers(@RequestBody Users users){
        return ResponseEntity.ok(userService.updateUsers(users));


    }







}
