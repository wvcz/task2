package kz.aitu.task2.controller;


import kz.aitu.task2.service.CaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseController {
   private final CaseService caseService;

    public CaseController(CaseService caseService) {
        this.caseService = caseService;
    }

    @GetMapping(path = "/api/cases/{id}")
    public ResponseEntity<?> getCase(@PathVariable Long id){
        return ResponseEntity.ok(caseService.getCase(id));
    }

    @GetMapping(path = "/api/cases")
    public ResponseEntity<?> getAllCase() {
        return ResponseEntity.ok(caseService.getAllCase());
    }

    @DeleteMapping(path = "/api/cases/{id}")
    public void deleteCaseById(@PathVariable long id){
        caseService.deleteCase(id);
    }




}
