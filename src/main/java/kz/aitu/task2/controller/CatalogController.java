package kz.aitu.task2.controller;

import kz.aitu.task2.entity.Catalog;
import kz.aitu.task2.entity.Catalog_case;
import kz.aitu.task2.repository.CatalogRepository;
import kz.aitu.task2.service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogController {
    private final CatalogService catalogService;

    public CatalogController(CatalogService catalogService) {
        this.catalogService = catalogService;
    }

    @GetMapping(path = "/api/catalog/{id}")
    public ResponseEntity<?> getCatalog(@PathVariable Long id){
        return ResponseEntity.ok(catalogService.getCatalog(id));
    }

    @GetMapping(path = "/api/catalog")
    public ResponseEntity<?> getAllCatalog() {
        return ResponseEntity.ok(catalogService.getAllCatalog());
    }

    @DeleteMapping(path = "/api/catalog/{id}")
    public void deleteCatalogById(@PathVariable long id){
        catalogService.deleteCatalog(id);
    }
    @PutMapping(path = "/api/catalog")
    public ResponseEntity<?>updateCatalog(@RequestBody Catalog catalog) {
        return ResponseEntity.ok(catalogService.updateCatalog(catalog));
    }
}
