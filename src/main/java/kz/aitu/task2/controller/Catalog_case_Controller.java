package kz.aitu.task2.controller;

import kz.aitu.task2.entity.Catalog_case;
import kz.aitu.task2.service.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public class Catalog_case_Controller {
    private final CatalogCaseService catalogCaseService;

    public Catalog_case_Controller(CatalogCaseService catalogCaseService) {
        this.catalogCaseService = catalogCaseService;
    }



    @GetMapping(path = "/api/catalogcase/{id}")
    public ResponseEntity<?> getCatalogCase(@PathVariable Long id){
        return ResponseEntity.ok(catalogCaseService.getCatalogCase(id));
    }

    @GetMapping(path = "/api/catalogcase")
    public ResponseEntity<?> getAllCatalogCase() {
        return ResponseEntity.ok(catalogCaseService.getAllCatalogCase());
    }

    @DeleteMapping(path = "/api/catalogcase/{id}")
    public void deleteCatalogCaseById(@PathVariable long id){
        catalogCaseService.deleteCatalogCase(id);
    }
    @PutMapping(path = "/api/catalogcase")
    public ResponseEntity<?>updateCatalogCase(@RequestBody Catalog_case catalog_case) {
        return ResponseEntity.ok(catalogCaseService.updateCatalogCase(catalog_case));
    }
}
