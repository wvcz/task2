package kz.aitu.task2.controller;

import kz.aitu.task2.entity.File;
import kz.aitu.task2.service.FileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FIleController {
    private final FileService fileService;

    public FIleController(FileService fileService) {
        this.fileService = fileService;
    }
    @GetMapping(path = "/api/file/{id}")
    public ResponseEntity<?> getFile(@PathVariable Long id){
        return ResponseEntity.ok(fileService.getFile(id));
    }

    @GetMapping(path = "/api/file")
    public ResponseEntity<?> getAllFile() {
        return ResponseEntity.ok(fileService.getAllFile());
    }

    @DeleteMapping(path = "/api/file/{id}")
    public void deleteFileById(@PathVariable long id){
        fileService.deleteFile(id);
    }
    @PutMapping(path = "/api/file")
    public ResponseEntity<?>updateFile(@RequestBody File file ){
        return ResponseEntity.ok(fileService.updateFile(file));
    }
}
