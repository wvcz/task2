package kz.aitu.task2.controller;

import kz.aitu.task2.entity.Case_index;

import kz.aitu.task2.service.CaseIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class Case_index_Controller {
    private final CaseIndexService caseIndexService;

    public Case_index_Controller(CaseIndexService caseIndexService) {
        this.caseIndexService = caseIndexService;
      
    }



    @GetMapping(path = "/api/caseindex/{id}")
    public ResponseEntity<?> getCaseIndex(@PathVariable Long id){
        return ResponseEntity.ok(caseIndexService.getCaseIndex(id));
    }

    @GetMapping(path = "/api/caseindex")
    public ResponseEntity<?> getAllCaseIndex() {
        return ResponseEntity.ok(caseIndexService.getAllCaseIndex());
    }


    @DeleteMapping(path = "/api/caseindex/{id}")
    public void deleteCaseIndexById(@PathVariable long id){
        caseIndexService.deleteCaseIndex(id);
    }
    @PutMapping(path = "/api/caseindex")
    public ResponseEntity<?>updateCaseIndex(@RequestBody Case_index case_index) {
        return ResponseEntity.ok(caseIndexService.updateCaseIndex(case_index));
    }


}
