package kz.aitu.task2.controller;


import kz.aitu.task2.entity.Tempfiles;
import kz.aitu.task2.service.TempfileService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TempfilesController {
    private final TempfileService tempfileService;

    public TempfilesController(TempfileService tempfileService) {
        this.tempfileService = tempfileService;
    }
    @GetMapping(path = "/api/tempfile/{id}")
    public ResponseEntity<?> getTempfile(@PathVariable Long id){
        return ResponseEntity.ok(tempfileService.getTempfile(id));
    }

    @GetMapping(path = "/api/tempfile")
    public ResponseEntity<?> getAllTempfile() {
        return ResponseEntity.ok(tempfileService.getAllTempfile());
    }

    @DeleteMapping(path = "/api/tempfile/{id}")
    public void deleteTempfileById(@PathVariable long id){
        tempfileService.deleteTempfile(id);
    }
    @PutMapping(path = "/api/tempfile")
    public ResponseEntity<?>updateTempfile(@RequestBody Tempfiles tempfiles ){
        return ResponseEntity.ok(tempfileService.updateTempfile(tempfiles));
    }
}
