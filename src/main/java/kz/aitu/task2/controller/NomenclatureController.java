package kz.aitu.task2.controller;


import kz.aitu.task2.entity.Nomenclature;
import kz.aitu.task2.service.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureController {
    private final NomenclatureService nomenclatureService;

    public NomenclatureController(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }
    @GetMapping(path = "/api/nomenclature/{id}")
    public ResponseEntity<?> getNomenclature(@PathVariable Long id){
        return ResponseEntity.ok(nomenclatureService.getNomenclature(id));
    }

    @GetMapping(path = "/api/nomenclature")
    public ResponseEntity<?> getAllNomenclature() {
        return ResponseEntity.ok(nomenclatureService.getAllNomenclature());
    }

    @DeleteMapping(path = "/api/nomenclature/{id}")
    public void deleteNomenclatureById(@PathVariable long id){
        nomenclatureService.deleteNomenclature(id);
    }
    @PutMapping(path = "/api/nomenclature")
    public ResponseEntity<?>updateNomenclature(@RequestBody Nomenclature nomenclature ){
        return ResponseEntity.ok(nomenclatureService.updateNomenclature(nomenclature));
    }
}
