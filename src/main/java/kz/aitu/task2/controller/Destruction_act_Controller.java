package kz.aitu.task2.controller;


import kz.aitu.task2.entity.Desrtuction_act;
import kz.aitu.task2.service.DestructionActService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class Destruction_act_Controller {
    private final DestructionActService destructionActService;

    public Destruction_act_Controller(DestructionActService destructionActService) {
        this.destructionActService = destructionActService;
    }

    @GetMapping(path = "/api/act/{id}")
    public ResponseEntity<?> getAct(@PathVariable Long id){
        return ResponseEntity.ok(destructionActService.getAct(id));
    }

    @GetMapping(path = "/api/act")
    public ResponseEntity<?> getAllAct() {
        return ResponseEntity.ok(destructionActService.getAllAct());
    }

    @DeleteMapping(path = "/api/act/{id}")
    public void deleteActById(@PathVariable long id){
        destructionActService.deleteAct(id);
    }
    @PutMapping(path = "/api/act")
    public ResponseEntity<?>updateAct(@RequestBody Desrtuction_act desrtuction_act) {
        return ResponseEntity.ok(destructionActService.updateAct(desrtuction_act));
    }
}
