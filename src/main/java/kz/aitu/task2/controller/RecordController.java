package kz.aitu.task2.controller;
import kz.aitu.task2.entity.Record;
import kz.aitu.task2.service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecordController {
    private final RecordService recordService;

    public RecordController(RecordService recordService) {
        this.recordService = recordService;
    }
    @GetMapping(path = "/api/record/{id}")
    public ResponseEntity<?> getRecord(@PathVariable Long id){
        return ResponseEntity.ok(recordService.getRecord(id));
    }

    @GetMapping(path = "/api/record")
    public ResponseEntity<?> getAllRecord() {
        return ResponseEntity.ok(recordService.getAllRecord());
    }

    @DeleteMapping(path = "/api/record/{id}")
    public void deleteRecordById(@PathVariable long id){
        recordService.deleteRecord(id);
    }
    @PutMapping(path = "/api/record")
    public ResponseEntity<?>updateRecord(@RequestBody Record record ){
        return ResponseEntity.ok(recordService.updateRecord(record));
    }
}
