package kz.aitu.task2.controller;

import kz.aitu.task2.entity.File;
import kz.aitu.task2.entity.Fond;
import kz.aitu.task2.repository.FondRepository;
import kz.aitu.task2.service.FondService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class FondController {
    private final FondService fondService;

    public FondController(FondService fondService) {
        this.fondService = fondService;
    }
    @GetMapping(path = "/api/fond/{fond_id}")
    public ResponseEntity<?> getFond(@PathVariable Long fond_id){
        return ResponseEntity.ok(fondService.getFond(fond_id));
    }

    @GetMapping(path = "/api/fond")
    public ResponseEntity<?> getAllFond() {
        return ResponseEntity.ok(fondService.getAllFond());
    }

    @DeleteMapping(path = "/api/fond/{id}")
    public void deleteFondById(@PathVariable long fond_id){
        fondService.deleteFile(fond_id);
    }
    @PutMapping(path = "/api/fond")
    public ResponseEntity<?>updateFond(@RequestBody Fond fond ){
        return ResponseEntity.ok(fondService.updateFond(fond));
    }
}
