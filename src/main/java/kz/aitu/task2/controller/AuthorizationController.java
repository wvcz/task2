package kz.aitu.task2.controller;

import kz.aitu.task2.entity.Authorization;
import kz.aitu.task2.service.AuthorizationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthorizationController {
    private final AuthorizationService authorizationService;

    public AuthorizationController(AuthorizationService authorizationService) {
        this.authorizationService = authorizationService;

    }


    @GetMapping(path = "/api/auths/{auth_id}")
    public ResponseEntity<?> getAuths(@PathVariable Long auth_id) {
        return ResponseEntity.ok(authorizationService.getAuths(auth_id));
    }


    @GetMapping(path = "/api/auths")
    public ResponseEntity<?> getAllAuths() {
        return ResponseEntity.ok(authorizationService.getAllAuths());
    }


    @DeleteMapping(path = "/api/auths/{auth_id}")
    public void deleteAuthById(@PathVariable long auth_id) {
        authorizationService.deleteAuths(auth_id);
    }

    @PutMapping(path = "/api/auths")
    public ResponseEntity<?> updateAuths(@RequestBody Authorization authorization) {
        return ResponseEntity.ok(authorizationService.updateAuths(authorization));


    }
}
