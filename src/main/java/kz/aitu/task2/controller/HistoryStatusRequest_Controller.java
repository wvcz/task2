package kz.aitu.task2.controller;

import kz.aitu.task2.entity.File;
import kz.aitu.task2.entity.HistoryStatusRequest;
import kz.aitu.task2.repository.HistoryStatusRequestRepository;
import kz.aitu.task2.service.HistoryStatusRequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class HistoryStatusRequest_Controller {
    private final HistoryStatusRequestService historyStatusRequestService;

    public HistoryStatusRequest_Controller(HistoryStatusRequestService historyStatusRequestService) {
        this.historyStatusRequestService = historyStatusRequestService;
    }
    @GetMapping(path = "/api/history/{id}")
    public ResponseEntity<?> getFile(@PathVariable Long id){
        return ResponseEntity.ok(historyStatusRequestService.getHistory(id));
    }

    @GetMapping(path = "/api/history")
    public ResponseEntity<?> getAllHistory() {
        return ResponseEntity.ok(historyStatusRequestService.getAllHistory());
    }

    @DeleteMapping(path = "/api/history/{id}")
    public void deleteHistoryById(@PathVariable long id){
        historyStatusRequestService.deleteHistory(id);
    }
    @PutMapping(path = "/api/history")
    public ResponseEntity<?>updateHistory(@RequestBody HistoryStatusRequest historyStatusRequest ){
        return ResponseEntity.ok(historyStatusRequestService.updateHistory(historyStatusRequest));
    }


}
