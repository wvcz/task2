package kz.aitu.task2.controller;

import kz.aitu.task2.entity.Request;
import kz.aitu.task2.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestController {
    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }
    @GetMapping(path = "/api/request/{ request_id}")
    public ResponseEntity<?> getRequest(@PathVariable Long  request_id){
        return ResponseEntity.ok(requestService.getRequest( request_id));
    }

    @GetMapping(path = "/api/request")
    public ResponseEntity<?> getAllRequest() {
        return ResponseEntity.ok(requestService.getAllRequest());
    }

    @DeleteMapping(path = "/api/request/{ request_id}")
    public void deleteRequestById(@PathVariable long  request_id){
        requestService.deleteRequest( request_id);
    }
    @PutMapping(path = "/api/request")
    public ResponseEntity<?>updateRequest(@RequestBody Request request ){
        return ResponseEntity.ok(requestService.updateRequest(request));
    }
}
