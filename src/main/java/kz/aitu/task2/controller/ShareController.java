package kz.aitu.task2.controller;


import kz.aitu.task2.entity.Share;
import kz.aitu.task2.service.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShareController {
   private final ShareService shareService;

    public ShareController(ShareService shareService) {
        this.shareService = shareService;
    }
    @GetMapping(path = "/api/share/{id}")
    public ResponseEntity<?> getShare(@PathVariable Long id){
        return ResponseEntity.ok(shareService.getShare(id));
    }

    @GetMapping(path = "/api/share")
    public ResponseEntity<?> getAllShare() {
        return ResponseEntity.ok(shareService.getAllShare());
    }

    @DeleteMapping(path = "/api/share/{id}")
    public void deleteShareById(@PathVariable long id){
        shareService.deleteShare(id);
    }
    @PutMapping(path = "/api/share")
    public ResponseEntity<?>updateShare(@RequestBody Share share ){
        return ResponseEntity.ok(shareService.updateShare(share));
    }
}
