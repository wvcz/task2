package kz.aitu.task2.controller;


import kz.aitu.task2.entity.Location;
import kz.aitu.task2.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationController { 
    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }
    @GetMapping(path = "/api/location/{location_id}")
    public ResponseEntity<?> getLocation(@PathVariable Long location_id){
        return ResponseEntity.ok(locationService.getLocation(location_id));
    }

    @GetMapping(path = "/api/location")
    public ResponseEntity<?> getAllLocation() {
        return ResponseEntity.ok(locationService.getAllLocation());
    }

    @DeleteMapping(path = "/api/location/{location_id}")
    public void deleteLocationById(@PathVariable long location_id){
        locationService.deleteLocation(location_id);
    }
    @PutMapping(path = "/api/location")
    public ResponseEntity<?>updateLocation(@RequestBody Location location ){
        return ResponseEntity.ok(locationService.updateLocation(location));
    }
}
