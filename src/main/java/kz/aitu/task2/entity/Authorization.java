package kz.aitu.task2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "authorizationn")
public class Authorization {
    @Id
    private long auth_id;
    @Column
    private String username;
    private String password;
    private String role;
    private String forgot_password_key;
    private long forgot_password_key_timestamp;
    private long company_unit_id;



}
