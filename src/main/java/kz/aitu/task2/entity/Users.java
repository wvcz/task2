package kz.aitu.task2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class Users {
    @Id
    private long user_id;
    @Column
    private long auth_id;
    private String name;
    private String fullname;
    private String secondname;
    private String status;
    private long company_unit_id;
    private String password;
    private long last_login_timestamp;
    private String iin;
    private boolean is_active;
    private boolean is_activated;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;


}
