package kz.aitu.task2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "file_routing")
public class File_routing {
    @Id
    private long id;
    @Column
    private long file_id;
    private String table_name;
    private long table_id;
    private String type;
}
