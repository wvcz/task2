package kz.aitu.task2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "tempfiles")
public class Tempfiles {
    @Id
    private int id ;
    @Column(columnDefinition = "text")
    private String file_binary;
    private byte file_binary_byte;

}
