package kz.aitu.task2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cases")

public class Case {
    @Id
    private long id;
    @Column
    private String case_number;
    private String volume_number;
    private String title_ru;
    private String title_kz;
    private String title_en;
    private long start_date;
    private long finish_date;
    private long page_count;
    private boolean subscription_eds;
    private String eds;
    private boolean dekivery_naf;
    private boolean is_deleted;
    private boolean limited_access;
    private String hash;
    private int version;
    private  String version_id;
    private boolean is_active_version;
    private String note;
    private long location_id;
    private long case_idex_id;
    private long record;
    private long destruction_act;
    private long company_unit_id;
    private String blockchain_address;
    private  long blockchain_include_date;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;
}
