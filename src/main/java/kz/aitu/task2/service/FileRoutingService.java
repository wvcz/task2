package kz.aitu.task2.service;

import kz.aitu.task2.entity.File_routing;
import kz.aitu.task2.entity.File_routing;
import kz.aitu.task2.repository.DestructionActRepository;
import kz.aitu.task2.repository.File_routing_Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class FileRoutingService {
    File_routing_Repository file_routing_repository;

    public FileRoutingService(File_routing_Repository file_routing_repository) {
        this.file_routing_repository = file_routing_repository;
    }


    public Optional<File_routing> getFileRouting(long id) {
        return file_routing_repository.findById(id);
    }

    public List<File_routing> getAllFileRouting() {
        return (List<File_routing>) file_routing_repository.findAll();
    }

    public void deleteFileRouting(long id) {
        file_routing_repository.deleteById(id);
    }

    public File_routing updateFileRouting(@RequestBody File_routing file_routing) {
        return file_routing_repository.save(file_routing);

    }

}
