package kz.aitu.task2.service;

import kz.aitu.task2.entity.Catalog_case;
import kz.aitu.task2.repository.Catalog_case_Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class CatalogCaseService {
    Catalog_case_Repository catalog_case_repository;

    public CatalogCaseService(Catalog_case_Repository catalog_case_repository) {
        this.catalog_case_repository = catalog_case_repository;
    }


    public Optional<Catalog_case> getCatalogCase(long id) {
        return catalog_case_repository.findById(id);
    }

    public List<Catalog_case> getAllCatalogCase() {
        return (List<Catalog_case>) catalog_case_repository.findAll();
    }

    public void deleteCatalogCase(long id) {
        catalog_case_repository.deleteById(id);

    }

    public Catalog_case updateCatalogCase(@RequestBody Catalog_case catalog_case) {
        return catalog_case_repository.save(catalog_case);

    }
}
