package kz.aitu.task2.service;

import kz.aitu.task2.entity.Location;
import kz.aitu.task2.repository.LocationRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class LocationService {
    LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }
    public Optional<Location> getLocation(long location_id) {
        return locationRepository.findById(location_id);
    }

    public List<Location> getAllLocation() {
        return (List<Location>) locationRepository.findAll();
    }

    public void deleteLocation(long location_id) {
        locationRepository.deleteById(location_id);
    }

    public Location updateLocation(@RequestBody Location location) {
        return locationRepository.save(location);

    }

}
