package kz.aitu.task2.service;


import kz.aitu.task2.entity.Search_key_routing;
import kz.aitu.task2.repository.SearchKeyRoutingRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class SearchKeyRoutingService {
    SearchKeyRoutingRepository searchKeyRoutingRepository;

    public SearchKeyRoutingService(SearchKeyRoutingRepository searchKeyRoutingRepository) {
        this.searchKeyRoutingRepository = searchKeyRoutingRepository;
    }


        public Optional<Search_key_routing> getSearchKeyRouting(long id ) {
            return searchKeyRoutingRepository.findById(id);
        }


        public List<Search_key_routing> getAllSearchKeyRouting() {
            return (List<Search_key_routing>) searchKeyRoutingRepository.findAll();
        }

        public void deleteSearchKeyRouting(long id) {
            searchKeyRoutingRepository.deleteById(id);
        }

        public Search_key_routing updateSearchKeyRouting(@RequestBody Search_key_routing search_key_routing) {
            return searchKeyRoutingRepository.save(search_key_routing);

        }

}
