package kz.aitu.task2.service;



import kz.aitu.task2.entity.Users;

import kz.aitu.task2.repository.UsersRepository;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    UsersRepository usersRepository;

    public UserService(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }
    public Optional<Users>getUsers(long user_id){
        return usersRepository.findById(user_id);
    }
    public List<Users>getAllUsers(){
        return(List<Users>) usersRepository.findAll();
    }
    public void deleteUser(long user_id){
        usersRepository.deleteById(user_id);

    }
    public Users updateUsers(@RequestBody Users users){
        return usersRepository.save(users);

    }


}
