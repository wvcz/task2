package kz.aitu.task2.service;

import kz.aitu.task2.entity.File;
import kz.aitu.task2.entity.Tempfiles;
import kz.aitu.task2.repository.TempfilesRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class TempfileService {
    TempfilesRepository tempfilesRepository;

    public TempfileService(TempfilesRepository tempfilesRepository) {
        this.tempfilesRepository = tempfilesRepository;

    }
    public Optional<Tempfiles> getTempfile(long id) {
        return tempfilesRepository.findById(id);
    }

    public List<Tempfiles> getAllTempfile() {
        return (List<Tempfiles>) tempfilesRepository.findAll();
    }

    public void deleteTempfile(long id) {
        tempfilesRepository.deleteById(id);
    }

    public Tempfiles updateTempfile(@RequestBody Tempfiles tempfiles) {
        return tempfilesRepository.save(tempfiles);

    }
}
