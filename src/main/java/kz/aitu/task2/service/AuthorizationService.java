package kz.aitu.task2.service;

import kz.aitu.task2.entity.Authorization;
import kz.aitu.task2.entity.Users;
import kz.aitu.task2.repository.AuthorizationRepository;
import kz.aitu.task2.repository.UsersRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class AuthorizationService {
    AuthorizationRepository authorizationRepository;

    public AuthorizationService(AuthorizationRepository authorizationRepository) {
        this.authorizationRepository = authorizationRepository;
    }

    public Optional<Authorization> getAuths(long auth_id){
        return authorizationRepository.findById(auth_id);
    }
    public List<Authorization> getAllAuths(){
        return(List<Authorization>) authorizationRepository.findAll();
    }
    public void deleteAuths(long auth_id){
        authorizationRepository.deleteById(auth_id);

    }
    public Authorization updateAuths(@RequestBody Authorization authorization){
        return authorizationRepository.save(authorization);

    }


}
