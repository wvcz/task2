package kz.aitu.task2.service;


import kz.aitu.task2.entity.Record;
import kz.aitu.task2.repository.RecordRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class RecordService {
    RecordRepository recordRepository;

    public RecordService(RecordRepository recordRepository) {
        this.recordRepository = recordRepository;
    }
    public Optional<Record> getRecord(long id) {
        return recordRepository.findById(id);
    }

    public List<Record> getAllRecord() {
        return (List<Record>) recordRepository.findAll();
    }

    public void deleteRecord(long id) {recordRepository.deleteById(id);
    }

    public Record updateRecord(@RequestBody Record record) {
        return recordRepository.save(record);

    }

}
