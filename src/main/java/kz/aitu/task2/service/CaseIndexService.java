package kz.aitu.task2.service;

import kz.aitu.task2.entity.Activity_journal;
import kz.aitu.task2.entity.Case_index;
import kz.aitu.task2.repository.Activity_journal_Repository;
import kz.aitu.task2.repository.Case_index_Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class CaseIndexService {
    Case_index_Repository case_index_repository;

    public CaseIndexService(Case_index_Repository case_index_repository) {
        this.case_index_repository = case_index_repository;
    }

    public Optional<Case_index> getCaseIndex(long id){
        return case_index_repository.findById(id);
    }
    public List<Case_index> getAllCaseIndex(){
        return(List<Case_index>) case_index_repository.findAll();
    }
    public void deleteCaseIndex(long id){
        case_index_repository.deleteById(id);

    }
    public Case_index updateCaseIndex(@RequestBody Case_index case_index){
        return case_index_repository.save(case_index);

    }
}
