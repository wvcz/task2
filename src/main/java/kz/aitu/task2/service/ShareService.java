package kz.aitu.task2.service;

import kz.aitu.task2.entity.Share;
import kz.aitu.task2.repository.ShareRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class ShareService {
    ShareRepository shareRepository;

    public ShareService(ShareRepository shareRepository) {
        this.shareRepository = shareRepository;
    }
    public Optional<Share> getShare(long id) {

        return shareRepository.findById(id);
    }

    public List<Share> getAllShare() {

        return (List<Share>) shareRepository.findAll();
    }

    public void deleteShare(long id) {
        shareRepository.deleteById(id);
    }

    public Share updateShare(@RequestBody Share share) {
        return shareRepository.save(share);

    }
}
