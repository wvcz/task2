package kz.aitu.task2.service;


import kz.aitu.task2.entity.Company_unit;
import kz.aitu.task2.repository.Company_unit_Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyUnitService {
    Company_unit_Repository company_unit_repository;

    public CompanyUnitService(Company_unit_Repository company_unit_repository) {
        this.company_unit_repository = company_unit_repository;
    }




    public Optional<Company_unit> getCompanyUnit(long company_unit_id) {
        return company_unit_repository.findById(company_unit_id);
    }

    public List<Company_unit> getAllCompanyUnit() {
        return (List<Company_unit>) company_unit_repository.findAll();
    }

    public void deleteCompanyUnit(long company_unit_id) {
        company_unit_repository.deleteById(company_unit_id);

    }

    public Company_unit updateCompanyUnit(@RequestBody Company_unit company_unit) {
        return company_unit_repository.save(company_unit);

    }
}
