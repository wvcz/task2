package kz.aitu.task2.service;

import kz.aitu.task2.entity.Notification;
import kz.aitu.task2.repository.NotificationRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class NotificationService {
    NotificationRepository notificationRepository;

    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }
    public Optional<Notification> getNotification(long id) {
        return notificationRepository.findById(id);
    }

    public List<Notification> getAllNotification() {
        return (List<Notification>) notificationRepository.findAll();
    }

    public void deleteNotification(long id) {
        notificationRepository.deleteById(id);
    }

    public Notification updateNotification(@RequestBody Notification notification) {
        return notificationRepository.save(notification);

    }


}
