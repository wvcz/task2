package kz.aitu.task2.service;

import kz.aitu.task2.entity.Fond;
import kz.aitu.task2.repository.FileRepository;
import kz.aitu.task2.repository.FondRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class FondService {
    FondRepository fondRepository;

    public FondService(FondRepository fondRepository) {
        this.fondRepository = fondRepository;
    }

    public Optional<Fond> getFond(long id) {
        return fondRepository.findById(id);
    }

    public List<Fond> getAllFond() {
        return (List<Fond>) fondRepository.findAll();
    }

    public void deleteFile(long id) {
        fondRepository.deleteById(id);
    }

    public Fond updateFond(@RequestBody Fond fond) {
        return fondRepository.save(fond);

    }
}
