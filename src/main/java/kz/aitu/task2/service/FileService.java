package kz.aitu.task2.service;

import kz.aitu.task2.entity.File;
import kz.aitu.task2.repository.FileRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;


@Service
public class FileService {
    FileRepository fileRepository;

    public FileService(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }


    public Optional<File> getFile(long id) {
        return fileRepository.findById(id);
    }

    public List<File> getAllFile() {
        return (List<File>) fileRepository.findAll();
    }

    public void deleteFile(long id) {
        fileRepository.deleteById(id);
    }

    public File updateFile(@RequestBody File file) {
        return fileRepository.save(file);

    }

    }










