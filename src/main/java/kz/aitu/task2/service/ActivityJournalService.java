package kz.aitu.task2.service;

import kz.aitu.task2.entity.Activity_journal;
import kz.aitu.task2.repository.Activity_journal_Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class ActivityJournalService {
    Activity_journal_Repository activity_journal_repository;

    public ActivityJournalService(Activity_journal_Repository activity_journal_repository) {
        this.activity_journal_repository = activity_journal_repository;
    }


    public Optional<Activity_journal> getJournals(long id){
        return activity_journal_repository.findById(id);
    }
    public List<Activity_journal> getAllJournals(){
        return(List<Activity_journal>) activity_journal_repository.findAll();
    }
    public void deleteJournal(long id){
        activity_journal_repository.deleteById(id);

    }
    public Activity_journal updateJournal(@RequestBody Activity_journal activity_journal){
        return activity_journal_repository.save(activity_journal);

    }
}
