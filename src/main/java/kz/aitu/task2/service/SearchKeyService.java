package kz.aitu.task2.service;

import kz.aitu.task2.entity.Searchkey;
import kz.aitu.task2.repository.SearchKeyRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class SearchKeyService {
    SearchKeyRepository searchKeyRepository;

    public SearchKeyService(SearchKeyRepository searchKeyRepository) {
        this.searchKeyRepository = searchKeyRepository;
    }

    public Optional<Searchkey> getSearchKey(long id ) {
        return searchKeyRepository.findById(id);
    }


    public List<Searchkey> getAllSearchKey() {
        return (List<Searchkey>) searchKeyRepository.findAll();
    }

    public void deleteSearchKey(long id) {
        searchKeyRepository.deleteById(id);
    }

    public Searchkey updateSearchKey(@RequestBody Searchkey search_key_routing) {
        return searchKeyRepository.save(search_key_routing);

    }
}
