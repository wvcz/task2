package kz.aitu.task2.service;

import kz.aitu.task2.entity.Nomenclature;
import kz.aitu.task2.repository.NomenclatureRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class NomenclatureService {
    NomenclatureRepository nomenclatureRepository;

    public NomenclatureService(NomenclatureRepository nomenclatureRepository) {
        this.nomenclatureRepository = nomenclatureRepository;
    }
    public Optional<Nomenclature> getNomenclature(long id) {
        return nomenclatureRepository.findById(id);
    }

    public List<Nomenclature> getAllNomenclature() {
        return (List<Nomenclature>) nomenclatureRepository.findAll();
    }

    public void deleteNomenclature(long id) {
        nomenclatureRepository.deleteById(id);
    }

    public Nomenclature updateNomenclature(@RequestBody Nomenclature file) {
        return nomenclatureRepository.save(file);

    }

}
