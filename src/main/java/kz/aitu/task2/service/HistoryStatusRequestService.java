package kz.aitu.task2.service;

import kz.aitu.task2.entity.HistoryStatusRequest;
import kz.aitu.task2.repository.HistoryStatusRequestRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class HistoryStatusRequestService {
    HistoryStatusRequestRepository historyStatusRequestRepository;

    public HistoryStatusRequestService(HistoryStatusRequestRepository historyStatusRequestRepository) {
        this.historyStatusRequestRepository = historyStatusRequestRepository;
    }
    public Optional<HistoryStatusRequest> getHistory(long id) {
        return historyStatusRequestRepository.findById(id);
    }

    public List<HistoryStatusRequest> getAllHistory() {
        return (List<HistoryStatusRequest>) historyStatusRequestRepository.findAll();
    }

    public void deleteHistory(long id) {
        historyStatusRequestRepository.deleteById(id);
    }

    public HistoryStatusRequest updateHistory(@RequestBody HistoryStatusRequest historyStatusRequest) {
        return historyStatusRequestRepository.save(historyStatusRequest);

    }

}
