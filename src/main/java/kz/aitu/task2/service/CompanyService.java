package kz.aitu.task2.service;

import kz.aitu.task2.entity.Company;
import kz.aitu.task2.repository.CompanyRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {
    CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }
    public Optional<Company> getCompany(long company_id) {
        return companyRepository.findById(company_id);
    }

    public List<Company> getAllCompany() {
        return (List<Company>) companyRepository.findAll();
    }

    public void deleteCompany(long company_id) {
        companyRepository.deleteById(company_id);

    }

    public Company updateCompany(@RequestBody Company company) {
        return companyRepository.save(company);

    }
}
