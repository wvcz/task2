package kz.aitu.task2.service;

import kz.aitu.task2.entity.Desrtuction_act;
import kz.aitu.task2.repository.DestructionActRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class DestructionActService {
    DestructionActRepository destructionActRepository;

    public DestructionActService(DestructionActRepository destructionActRepository) {
        this.destructionActRepository = destructionActRepository;
    }
    public Optional<Desrtuction_act> getAct(long id) {
        return destructionActRepository.findById(id);
    }

    public List<Desrtuction_act> getAllAct() {
        return (List<Desrtuction_act>) destructionActRepository.findAll();
    }

    public void deleteAct(long id) {
        destructionActRepository.deleteById(id);

    }

    public Desrtuction_act updateAct(@RequestBody Desrtuction_act desrtuction_act) {
        return destructionActRepository.save(desrtuction_act);

    }
}
