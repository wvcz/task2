package kz.aitu.task2.service;

import kz.aitu.task2.entity.Case;
import kz.aitu.task2.repository.CaseRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CaseService {
    CaseRepository caseRepository;


    public CaseService(CaseRepository caseRepository) {
        this.caseRepository = caseRepository;
    }
    public Optional<Case> getCase(long id){
        return caseRepository.findById(id);
    }


    public List<Case> getAllCase(){
        return(List<Case>) caseRepository.findAll();
    }
    public void deleteCase(long id){
        caseRepository.deleteById(id);

    }

}

