package kz.aitu.task2.service;
import kz.aitu.task2.entity.Request;
import kz.aitu.task2.repository.RequestRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class RequestService {
    RequestRepository requestRepository;

    public RequestService(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    public Optional<Request> getRequest(long request_id) {
        return requestRepository.findById( request_id);
    }

    public List<Request> getAllRequest() {
        return (List<Request>) requestRepository.findAll();
    }

    public void deleteRequest(long request_id) {requestRepository.deleteById(request_id);
    }

    public Request updateRequest(@RequestBody Request request) {
        return requestRepository.save(request);

    }
}
