package kz.aitu.task2.service;

import kz.aitu.task2.entity.Nomenclature_summary;
import kz.aitu.task2.repository.Nomenclature_summary_Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class NomenclatureSummaryService {
    Nomenclature_summary_Repository nomenclature_summary_repository;

    public NomenclatureSummaryService(Nomenclature_summary_Repository nomenclature_summary_repository) {
        this.nomenclature_summary_repository = nomenclature_summary_repository;
    }


    public Optional<Nomenclature_summary> getNomenclatureSummary(long id) {
        return nomenclature_summary_repository.findById(id);
    }

    public List<Nomenclature_summary> getAllNomenclatureSummary() {
        return (List<Nomenclature_summary>) nomenclature_summary_repository.findAll();
    }

    public void deleteNomenclatureSummary(long id) {
        nomenclature_summary_repository.deleteById(id);
    }

    public Nomenclature_summary updateNomenclatureSummary(@RequestBody Nomenclature_summary nomenclature_summary) {
        return nomenclature_summary_repository.save(nomenclature_summary);

    }
}
