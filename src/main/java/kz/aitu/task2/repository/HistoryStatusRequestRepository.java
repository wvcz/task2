package kz.aitu.task2.repository;

import kz.aitu.task2.entity.HistoryStatusRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryStatusRequestRepository extends CrudRepository<HistoryStatusRequest, Long> {
}
