package kz.aitu.task2.repository;


import kz.aitu.task2.entity.Search_key_routing;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRoutingRepository extends CrudRepository<Search_key_routing, Long> {

}
