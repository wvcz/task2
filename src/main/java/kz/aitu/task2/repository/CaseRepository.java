package kz.aitu.task2.repository;

import kz.aitu.task2.entity.Case;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface CaseRepository extends CrudRepository<Case, Long> {
}
