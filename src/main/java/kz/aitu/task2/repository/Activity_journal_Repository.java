package kz.aitu.task2.repository;

import kz.aitu.task2.entity.Activity_journal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Activity_journal_Repository extends CrudRepository<Activity_journal, Long> {
}

