package kz.aitu.task2.repository;

import kz.aitu.task2.entity.Case_index;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Case_index_Repository extends CrudRepository<Case_index, Long> {
}
