package kz.aitu.task2.repository;

import kz.aitu.task2.entity.Searchkey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRepository extends CrudRepository<Searchkey, Long> {
}
