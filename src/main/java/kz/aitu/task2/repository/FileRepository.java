package kz.aitu.task2.repository;

import kz.aitu.task2.entity.File;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRepository extends CrudRepository<File, Long> {
    @Query(value = "select * from file order by file_id",nativeQuery = true)
    List<File> findAllFiles();
    @Query(value = "select * from file where file_id = ?",nativeQuery = true)
    List<File> findAllByFileId(long file_id);
    @Query(value = "delete from file where file_id = ?",nativeQuery = true)
    List<File> deleteFileById(long file_id);





}
