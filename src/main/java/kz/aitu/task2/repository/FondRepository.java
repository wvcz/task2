package kz.aitu.task2.repository;

import kz.aitu.task2.entity.Fond;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FondRepository extends CrudRepository<Fond, Long> {
}
