package kz.aitu.task2.repository;

import kz.aitu.task2.entity.Nomenclature_summary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Nomenclature_summary_Repository extends CrudRepository<Nomenclature_summary, Long> {
}
