package kz.aitu.task2.repository;

import kz.aitu.task2.entity.Catalog_case;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Catalog_case_Repository extends CrudRepository<Catalog_case, Long> {
}
