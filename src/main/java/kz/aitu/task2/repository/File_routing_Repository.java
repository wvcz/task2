package kz.aitu.task2.repository;

import kz.aitu.task2.entity.File_routing;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface File_routing_Repository extends CrudRepository<File_routing, Long> {
}
