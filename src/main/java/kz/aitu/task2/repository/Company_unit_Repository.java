package kz.aitu.task2.repository;

import kz.aitu.task2.entity.Company_unit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Company_unit_Repository extends CrudRepository<Company_unit, Long> {
}
