package kz.aitu.task2.repository;

import kz.aitu.task2.entity.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface LocationRepository extends CrudRepository<Location, Long> {
}
