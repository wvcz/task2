package kz.aitu.task2.repository;


import kz.aitu.task2.entity.Share;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShareRepository extends CrudRepository<Share, Long> {
}
