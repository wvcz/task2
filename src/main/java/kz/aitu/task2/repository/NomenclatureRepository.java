package kz.aitu.task2.repository;

import kz.aitu.task2.entity.Nomenclature;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclatureRepository extends CrudRepository<Nomenclature, Long> {
}
